var DropLab = require('./droplab')();
var DATA_TRIGGER = require('./constants').DATA_TRIGGER;

var setup = function() {
  var droplab = DropLab();
  require('./window')(function(w) {
    w.onload = function () {
      var dropdownTriggers = [].slice.apply(document.querySelectorAll('['+DATA_TRIGGER+']'));
      droplab.addHooks(dropdownTriggers).init();
    };
  });
  return droplab;
};

module.exports = setup();
